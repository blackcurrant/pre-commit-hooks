import re
import os
import csv
import argparse
import subprocess
from typing import Sequence


PYDANTIC_SETTINGS_REGEX = re.compile(r"class [^\n(:]+\(BaseSettings\):")
FIELD_REGEX = re.compile(r"^[A-Z0-9_]+ *:.+$")
FIELD_NAME_REGEX = re.compile(r"^(?P<name>[^: ]+):")
FIELD_DESCRIPTION_REGEX = re.compile(r"description *= *\(* *\"(?P<description>[^\"]+)")
FIELD_DEFAULT_REGEXES = [
    re.compile(r"default *= *(?P<default>[^,)]+)"),
    re.compile(r" *= *(?P<default>.+)$"),
]
FIELD_EXAMPLES_REGEX = re.compile(r"examples *= *\[(?P<examples>[^]]+)]")

UNKNOWN = "???"
RESULT_FILE = "settings.csv"
HEADERS = ["name", "description", "default", "examples"]


def extract_fields_from_settings(file_text: str) -> list[str]:
    fields, file_text = [], file_text.split("\n")
    for i in range(len(file_text)):
        if PYDANTIC_SETTINGS_REGEX.search(file_text[i]):
            for j in range(i + 1, len(file_text)):
                row = file_text[j].replace("'", '"')
                stripped_row = row.strip()
                if any([stripped_row.startswith(f"{key} ")for key in ("class", "def", "if")]):
                    break
                if row.startswith(" " * 4):
                    if len(row) - 4 > len(stripped_row) or stripped_row == ")":
                        fields[-1] = fields[-1] + " " + stripped_row
                    elif FIELD_REGEX.search(stripped_row):
                        fields.append(stripped_row)
            break
    return fields


def extract_name(field: str) -> str:
    name = FIELD_NAME_REGEX.search(field)
    return name.group("name").strip() if name else UNKNOWN


def extract_description(field: str) -> str:
    if "Field" not in field or "description" not in field:
        return ""
    field = field.replace('" "', " ").replace("' '", " ")
    description = FIELD_DESCRIPTION_REGEX.search(field)
    return description.group("description").strip() if description else UNKNOWN


def extract_default(field: str) -> str:
    if "=" not in field or ("Field" in field and "default" not in field):
        return ""
    default = UNKNOWN
    for regex in FIELD_DEFAULT_REGEXES:
        found = regex.search(field)
        if found:
            default = found.group("default").strip()
            quotes = ("'", '"')
            if default[0] in quotes:
                default = default[1:]
            if default[-1] in quotes:
                default = default[:-1]
            break
    return f'"{default}"'


def extract_examples(field: str) -> str:
    if "examples" not in field:
        return ""

    examples = UNKNOWN
    found = FIELD_EXAMPLES_REGEX.search(field)
    if found:
        examples = found.group("examples").strip()
        examples = re.sub(r"['\"] *, *['\"]", '"\n"', examples)
        if examples[0] == "'":
            examples = '"' + examples[1:]
        if examples[-1] == "'":
            examples = examples[:-1] + '"'
    return examples


def write_settings_to_csv(file_text: str, filename: str) -> None:
    fields = extract_fields_from_settings(file_text)

    rows = []
    for field in fields:
        name = extract_name(field)
        description = extract_description(field)
        default = extract_default(field)
        examples = extract_examples(field)
        rows.append([name, description, default, examples])

    with open(filename, "w", encoding="utf-8") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(HEADERS)
        writer.writerows(rows)


def read_file(filename: str) -> str:
    with open(filename, "r") as f:
        return f.read()


def is_file_contains_settings(file_text: str) -> bool:
    return bool(PYDANTIC_SETTINGS_REGEX.search(file_text))


def main(argv: Sequence[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*")
    args = parser.parse_args(argv)

    for filename in args.filenames:
        if filename.endswith(".py"):
            file_text = read_file(filename)
            if is_file_contains_settings(file_text):
                filename = os.path.join(os.path.dirname(filename), RESULT_FILE)
                write_settings_to_csv(file_text, filename)
                subprocess.call(["git", "add", filename])

    subprocess.call(
        ["git", "add", RESULT_FILE],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )
    raise SystemExit(0)


# def main():
#     filename = "../pre_commit_hooks/test_settings.py"
#     file_text = read_file(filename)
#     filename = os.path.join(os.path.dirname(filename), RESULT_FILE)
#     write_settings_to_csv(file_text, filename)
#
#
# if __name__ == "__main__":
#     main()
