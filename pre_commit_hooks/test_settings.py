from typing import Literal

from pydantic import Field
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DB_HOST: str = Field(default="localhost", examples=["localhost", "postgres"])
    DB_PORT: int = Field(default="")
    DB_USERNAME: str
    DB_PASSWORD: str = Field(default="postgres")
    DB_DATABASE: str = Field(default="postgres")
    DB_ECHO_POOL: Literal["debug"] | bool = Field(default=False)
    DB_POOL_SIZE: int = Field(default=10)
    DB_CONNECTION_RETRY_PERIOD_SEC: float = Field(default=5.0)
